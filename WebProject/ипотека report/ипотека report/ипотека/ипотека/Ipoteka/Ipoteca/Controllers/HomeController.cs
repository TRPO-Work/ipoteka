﻿using Ipoteca.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Ipoteka.lib;
namespace Ipoteca.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AnnuityPay()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AnnuityPay(int mortgage, int timeYear, double rate)
        {
           

            try
            {
                Ipotekka das = new Ipotekka();
                var result = das.PaymentAnnuitet(mortgage, timeYear, rate);

                IndexVIewModel indexVIewModel = new IndexVIewModel
                (
                    new ClassResult(result.FirstPay, result.MinimPay, result.Overpay, result.SumPays),
                    new PayValues(mortgage, timeYear, rate)

                );

                return View(indexVIewModel);

            }
            catch
            {

                return RedirectToAction(nameof(HomeController.Error), "Home");
            }

          
        }

        public IActionResult DifferentPay()
        {
            return View();
        }
        [HttpPost]
        public IActionResult DifferentPay(int mortgage, int timeYear, double rate)
        {
            Ipotekka das = new Ipotekka();
            var result = das.PaymentDiffer(mortgage, timeYear, rate);

            IndexVIewModel indexVIewModel = new IndexVIewModel
            (
                new ClassResult(result.FirstPay, result.MinimPay, result.Overpay, result.SumPays),
                new PayValues(mortgage, timeYear, rate)

            );

            return View(indexVIewModel);
        }
        public IActionResult Report(int mortgage, int timeYear, double rate, double firstPay, double minimPay, double overpay, double sumPays)
        {
            IndexVIewModel ReportVIewModel = new IndexVIewModel
            (
                new ClassResult(firstPay, minimPay, overpay, sumPays),
                new PayValues(mortgage, timeYear, rate)

            );
            return View(ReportVIewModel);
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}