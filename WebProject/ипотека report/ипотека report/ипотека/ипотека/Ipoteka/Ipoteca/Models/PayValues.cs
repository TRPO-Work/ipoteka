﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Ipoteca.Models
{
    public class PayValues
    {
        public PayValues(int mortgage, int timeYear, double rate)
        {
            Mortgage=mortgage;
            TimeYear=timeYear;
            Rate=rate;
        }
        public int Mortgage { get; set; }

        public int TimeYear { get ; set; }

        public double Rate { get; set; }

    }
}
