﻿namespace Ipoteca.Models
{
    public class IndexVIewModel
    {
        public ClassResult ClassResult { get; }
        public PayValues PayValues { get; }

        public IndexVIewModel(ClassResult classResult, PayValues payValues)
        {
            ClassResult = classResult;
            PayValues = payValues;
        }
    }
}
