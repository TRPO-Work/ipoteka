﻿namespace Ipoteca.Models
{
    public class ClassResult
    {
        public ClassResult(double firstPay, double minimPay, double overpay, double sumPays)
        {
            FirstPay = firstPay;
            MinimPay = minimPay;
            Overpay = overpay;
            SumPays = sumPays;
        }
        public double FirstPay { get; set; }
        public double MinimPay { get; set; }
        public double Overpay { get; set; }
        public double SumPays { get; set; }
    }
}
