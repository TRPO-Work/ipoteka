﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ipoteka.lib
{
    public class Ipotekka
    {
        public (double FirstPay, double MinimPay, double Overpay, double SumPays) PaymentAnnuitet(int Mortgage, int TimeYear, double Rate)//расчет аннуитетного платежа
        {
            if (Mortgage > 0 && TimeYear > 0 && Rate > 0)
            {
                int TimeMonth = TimeYear * 12;//Срок ипотеки, мес
                double RateMonth = Rate / 1200;//Месячная ставка
                double FPay = 0.3 * Mortgage;//Первый взнос
                double OSPay = Mortgage-FPay;
                double MinMPay = OSPay * (RateMonth / (1 - Math.Pow(1 + RateMonth, -TimeMonth)));//Мин аннуитетный ежемесячный платеж
                double AnOverpay = MinMPay * TimeMonth - OSPay; //проценты за весь срок, переплата 
                double Mortg = Mortgage + AnOverpay; // общая сумма выплат

                var resultA = (FirstPay:Math.Round(FPay, 2), MinimPay: Math.Round(MinMPay, 2), Overpay: Math.Round(AnOverpay, 2) , SumPays: Math.Round(Mortg, 2));
                return resultA;
            }
            else if (Mortgage <= 0)
            {
                throw new ArgumentException("Сумма кредита должна быть натуральным числом (больше 0)");
            }
            else if (TimeYear <= 0)
            {
                throw new ArgumentException("Срок ипотеки должен быть от года и более");
            }
            else
            {
                throw new ArgumentException("Ставка должна быть указана положительным числом");
            }
        }
        public (double FirstPay, double MinimPay, double Overpay, double SumPays) PaymentDiffer(int Mortgage, int TimeYear, double Rate)
        {
            {
                if (Mortgage > 0 && TimeYear > 0 && Rate > 0)
                {
                    int TimeMonthD = TimeYear * 12;//Срок ипотеки, мес
                    double RateMonthD = Rate / 1200;//Месячная ставка
                    double FPayD = 0.3 * Mortgage;//Первый взнос
                    double Mortg = Mortgage - FPayD; // сумма кредита после уплаты первого взноса
                    double DifMDebt = Mortg / TimeMonthD;//Дифференциальное ежемесячное погашение долга, фикс
                    double BalanceD = Mortg; //Остаток дифференцированный
                    double Pereplata = 0; // переплата за весь период
                    double PercentMD;//Проценты месячные по дифференциалу
                    double AllSymm; //Общая сумма выплат 

                    // Расчет платежей

                    for (int i = 1; i <= TimeMonthD; i++)
                    {
                        PercentMD = BalanceD * RateMonthD;
                        Pereplata += PercentMD;
                        BalanceD -= DifMDebt;
                    }
                    AllSymm = Mortgage+Pereplata;

                    var resultD = (FirstPay: Math.Round(FPayD, 2), MinimPay: Math.Round(DifMDebt, 2), Overpay: Math.Round(Pereplata, 2), SumPays: Math.Round(AllSymm, 2));
                    return resultD;
                }
                else if (Mortgage <= 0)
                {
                    throw new ArgumentException("Сумма кредита должна быть натуральным числом (больше 0)");
                }
                else if (TimeYear <= 0)
                {
                    throw new ArgumentException("Срок ипотеки должен быть от года и более");
                }
                else
                {
                    throw new ArgumentException("Ставка должна быть указана положительным числом");
                }
            }
        }
    }
}

