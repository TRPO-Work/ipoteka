using NUnit.Framework;
using Ipoteka.lib;
using System;

namespace Ipoteka.tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        //����������� ������
        [Test]
        public void OkZnachA()
        {
            const int Mortgage = 20000;
            const int TimeYear = 3;
            const double Rate = 12;
            const double fday = 6000.00;
            const double mpay = 465.00;
            const double opay = 2740.00;
            const double spay = 22740.00;

            var result = new Ipotekka().PaymentAnnuitet(Mortgage, TimeYear, Rate);

            Assert.AreEqual(fday, result.FirstPay, 0.01);
            Assert.AreEqual(mpay, result.MinimPay, 0.01);
            Assert.AreEqual(opay, result.Overpay, 0.01);
            Assert.AreEqual(spay, result.SumPays, 0.01);
        }
        [Test]
        public void OtrMortagage()
        {
            const int Mortgage = -20000;
            const int TimeYear = 3;
            const double Rate = 12;

            Assert.Throws<ArgumentException>(() => new Ipotekka().PaymentAnnuitet(Mortgage, TimeYear, Rate));
        }
        [Test]
        public void OtrTimeYear()
        {
            const int Mortgage = 20000;
            const int TimeYear = -3;
            const double Rate = 12;

            Assert.Throws<ArgumentException>(() => new Ipotekka().PaymentAnnuitet(Mortgage, TimeYear, Rate));
        }
        [Test]
        public void OtrRate()
        {
            const int Mortgage = 20000;
            const int TimeYear = 3;
            const double Rate = -12;

            Assert.Throws<ArgumentException>(() => new Ipotekka().PaymentAnnuitet(Mortgage, TimeYear, Rate));
        }
        [Test]
        public void NullRate()
        {
            const int Mortgage = 20000;
            const int TimeYear = 3;
            const double Rate = 0;

            Assert.Throws<ArgumentException>(() => new Ipotekka().PaymentAnnuitet(Mortgage, TimeYear, Rate));
        }
        //������������������ ������
        [Test]
        public void OkZnachD()
        {
            const int Mortgage = 20000;
            const int TimeYear = 3;
            const double Rate = 12;
            const double fday = 6000.00;
            const double mpay = 388.89;
            const double opay = 2590.00;
            const double spay = 22590.00;

            var resultD = new Ipotekka().PaymentDiffer(Mortgage, TimeYear, Rate);

            Assert.AreEqual(fday, resultD.FirstPay, 0.01);
            Assert.AreEqual(mpay, resultD.MinimPay, 0.01);
            Assert.AreEqual(opay, resultD.Overpay, 0.01);
            Assert.AreEqual(spay, resultD.SumPays, 0.01);
        }

    }
}