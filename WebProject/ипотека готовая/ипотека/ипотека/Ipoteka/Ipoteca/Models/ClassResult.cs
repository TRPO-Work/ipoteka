﻿namespace Ipoteca.Models
{
    public class ClassResult
    {
        public double FirstPay { get; set; }
        public double MinimPay { get; set; }
        public double Overpay { get; set; }
        public double SumPays { get; set; }
    }
}
