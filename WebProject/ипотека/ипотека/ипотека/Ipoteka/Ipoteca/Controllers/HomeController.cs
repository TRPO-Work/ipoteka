﻿using Ipoteca.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Ipoteka.lib;
namespace Ipoteca.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Index2()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index2(int mortgage,int timeYear,double rate)
        {
           

            try
            {
                Ipotekka das = new Ipotekka();
                var result = das.PaymentAnnuitet(mortgage, timeYear, rate);

                ClassResult classResult = new ClassResult
                {
                    FirstPay = result.FirstPay,
                    MinimPay = result.MinimPay,
                    Overpay = result.Overpay,
                    SumPays = result.SumPays
                };
                return View(classResult);

            }
            catch
            {

                return RedirectToAction(nameof(HomeController.Error), "Home");
            }

          
        }

        public IActionResult Index3()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index3(int mortgage, int timeYear, double rate)
        {
            Ipotekka das = new Ipotekka();
            var result = das.PaymentAnnuitet(mortgage, timeYear, rate);

            ClassResult classResult = new ClassResult
            {
                FirstPay = result.FirstPay,
                MinimPay = result.MinimPay,
                Overpay = result.Overpay,
                SumPays = result.SumPays
            };

            return View(classResult);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}