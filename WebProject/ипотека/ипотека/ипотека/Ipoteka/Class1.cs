﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ipoteka.lib
{
 //   public class Results
 //   {
 //       public double FirstPay { get; set; }
 //       public double MinimPay { get; set; }
   //     public double Overpay { get; set; }
     //   public double SumPays { get; set; }
    //}
    public class Ipotekka
    {
        public (double FirstPay, double MinimPay, double Overpay, double SumPays) PaymentAnnuitet (int Mortgage, int TimeYear, double Rate)//расчет аннуитетного платежа
        {
            if (Mortgage > 0 && TimeYear > 0 && Rate > 0)
            {
                //var resultA = new Results();
                int TimeMonth = TimeYear * 12;//Срок ипотеки, мес
                double RateMonth = Math.Round(Rate / 1200,2);//Месячная ставка
                double FPay = Math.Round(0.3 * Mortgage,2);//Первый взнос
                double MinMPay = Math.Round(Mortgage * (RateMonth / (1 - Math.Pow(1 + RateMonth, -Mortgage))),2);//Мин аннуитетный ежемесячный платеж
                double AnOverpay = Math.Round(MinMPay * TimeMonth - Mortgage,2); //проценты за весь срок, переплата 
                double Mortg = Mortgage + AnOverpay; // общая сумма выплат
                                                     //resultA.FirstPay = FPay;
                                                     // resultA.MinimPay = MinMPay;
                                                     // resultA.Overpay = AnOverpay;
                                                     // resultA.SumPays = Mortg;
                var resultA = (FirstPay: FPay, MinimPay: MinMPay, Overpay: AnOverpay, SumPays: Mortg);
                return resultA;
            }
            else if (Mortgage <= 0)
            {
                throw new ArgumentException("Сумма кредита должна быть натуральным числом (больше 0)");
            }
            else if (TimeYear <= 0)
            {
                throw new ArgumentException("Срок ипотеки должен быть от года и более");
            }
            else
            {
                throw new ArgumentException("Ставка должна быть указана положительным числом");
            }
        }
        public (double FirstPay, double MinimPay, double Overpay, double SumPays) PaymentDiffer(int Mortgage, int FPay, int TimeYear, double Rate)
        {
            {
                if (Mortgage > 0 && TimeYear > 0 && Rate > 0)
                {
                    //var resultD = new Results();
                    int TimeMonthD = TimeYear * 12;//Срок ипотеки, мес
                    double RateMonthD = Math.Round(Rate / 1200, 2);//Месячная ставка
                    double FPayD = Math.Round(0.3 * Mortgage, 2);//Первый взнос
                    double DifMDebt = Math.Round((Mortgage-FPayD)/TimeMonthD, 2);//Дифференциальное ежемесячное погашение долга, фикс
                    double Mortg = Mortgage - FPayD; // сумма кредита после уплаты первого взноса
                    double BalanceD = Mortg; //Остаток дифференцированный
                    double Pereplata=0; // переплата за весь период
                    double DifMPay;//Дифференциальный ежемесячный платеж
                    double PercentMD;//Проценты месячные по дифференциалу
                    double AllSymm = FPayD; //Общая сумма выплат 

                    // Расчет платежей

                    for (int i = 1; i <= TimeMonthD; i++)
                    {
                        PercentMD = Math.Round(BalanceD * RateMonthD, 2);
                        Pereplata += PercentMD;
                        DifMPay = PercentMD + DifMDebt;
                        AllSymm += DifMPay;
                        BalanceD = BalanceD - DifMPay;
                    }
                    //resultD.FirstPay = FPayD;
                    //resultD.MinimPay = DifMDebt;
                    //resultD.Overpay = Pereplata;
                    //resultD.SumPays = AllSymm;
                    var resultD = (FirstPay: FPayD, MinimPay: DifMDebt, Overpay: Pereplata, SumPays: AllSymm);
                    return resultD;
                }
                else if (Mortgage <= 0)
                {
                    throw new ArgumentException("Сумма кредита должна быть натуральным числом (больше 0)");
                }
                else if (TimeYear <= 0)
                {
                    throw new ArgumentException("Срок ипотеки должен быть от года и более");
                }
                else
                {
                    throw new ArgumentException("Ставка должна быть указана положительным числом");
                }
            }
        }
    }
}
