﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ipoteka.lib
{
    public class Ipotekka
    {
        public (double FirstPay, double MinimPay, double Overpay, double SumPays) PaymentAnnuitet(int Mortgage, int TimeYear, double Rate)//расчет аннуитетного платежа
        {
            if (Mortgage > 0 && TimeYear > 0 && Rate > 0)
            {
                int TimeMonth = TimeYear * 12;//Срок ипотеки, мес
                double RateMonth = Math.Round(Rate / 1200, 2);//Месячная ставка
                double FPay = Math.Round(0.3 * Mortgage, 2);//Первый взнос
                double OSPay = Mortgage-FPay;
                double MinMPay = Math.Round(OSPay * (RateMonth / (1 - Math.Pow(1 + RateMonth, -TimeMonth))), 2);//Мин аннуитетный ежемесячный платеж
                double AnOverpay = Math.Round(MinMPay * TimeMonth - OSPay, 2); //проценты за весь срок, переплата 
                double Mortg = Mortgage + AnOverpay; // общая сумма выплат

                var resultA = (FirstPay: FPay, MinimPay: MinMPay, Overpay: AnOverpay, SumPays: Mortg);
                return resultA;
            }
            else if (Mortgage <= 0)
            {
                throw new ArgumentException("Сумма кредита должна быть натуральным числом (больше 0)");
            }
            else if (TimeYear <= 0)
            {
                throw new ArgumentException("Срок ипотеки должен быть от года и более");
            }
            else
            {
                throw new ArgumentException("Ставка должна быть указана положительным числом");
            }
        }
        public (double FirstPay, double MinimPay, double Overpay, double SumPays) PaymentDiffer(int Mortgage, int TimeYear, double Rate)
        {
            {
                if (Mortgage > 0 && TimeYear > 0 && Rate > 0)
                {
                    int TimeMonthD = TimeYear * 12;//Срок ипотеки, мес
                    double RateMonthD = Math.Round(Rate / 1200, 2);//Месячная ставка
                    double FPayD = Math.Round(0.3 * Mortgage, 2);//Первый взнос
                    double Mortg = Mortgage - FPayD; // сумма кредита после уплаты первого взноса
                    double DifMDebt = Math.Round(Mortg / TimeMonthD, 2);//Дифференциальное ежемесячное погашение долга, фикс
                    double BalanceD = Mortg; //Остаток дифференцированный
                    double Pereplata = 0; // переплата за весь период
                    double PercentMD;//Проценты месячные по дифференциалу
                    double AllSymm; //Общая сумма выплат 

                    // Расчет платежей

                    for (int i = 1; i <= TimeMonthD; i++)
                    {
                        PercentMD = Math.Round(BalanceD * RateMonthD, 2);
                        Pereplata += PercentMD;
                        BalanceD -= DifMDebt;
                    }
                    AllSymm = Mortgage+Pereplata;

                    var resultD = (FirstPay: FPayD, MinimPay: DifMDebt, Overpay: Pereplata, SumPays: AllSymm);
                    return resultD;
                }
                else if (Mortgage <= 0)
                {
                    throw new ArgumentException("Сумма кредита должна быть натуральным числом (больше 0)");
                }
                else if (TimeYear <= 0)
                {
                    throw new ArgumentException("Срок ипотеки должен быть от года и более");
                }
                else
                {
                    throw new ArgumentException("Ставка должна быть указана положительным числом");
                }
            }
        }
    }
}

